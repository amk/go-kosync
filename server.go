// go-kosync: a go implementation of the koreader document sync server
// Copyright (C) 2022  Alex McGrath

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"context"
	"crypto/subtle"
	"database/sql"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/gorilla/mux"

	_ "modernc.org/sqlite"
)

type Document struct {
	Document   string  `json:"document"`
	Percentage float64 `json:"percentage"`
	Progress   string  `json:"progress"`
	Device     string  `json:"device"`
	DeviceID   string  `json:"device_id"`
	Timestamp  int64   `json:"timestamp"`
}

type UpdateResponse struct {
	Document  Document `json:"document"`
	Timestamp int64    `json:"timestamp"`
}

func MigrateDB(db *sql.DB) error {
	_, err := db.Exec(
		"CREATE TABLE IF NOT EXISTS books(" +
			"document TEXT UNIQUE, " +
			"percentage REAL, " +
			"progress TEXT, " +
			"device TEXT, " +
			"device_id_field TEXT, " +
			"timestamp TEXT)")
	return err
}

type Config struct {
	UserName string `toml:"username"`
	Pass     string `toml:"md5token"`
	DBPath   string `toml:"db_path"`
}

type Server struct {
	config Config
	db     *sql.DB
}

type Authed struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type authCtx int

const authCtxID authCtx = iota

func (s *Server) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		username := r.Header.Get("x-auth-user")
		if s.config.UserName != username {
			rw.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(rw).Encode(Authed{
				Code:    2001,
				Message: "Unauthorized",
			})
			return
		}
		passmd5 := r.Header.Get("x-auth-key")
		if subtle.ConstantTimeCompare([]byte(s.config.Pass), []byte(passmd5)) != 1 {
			rw.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(rw).Encode(Authed{
				Code:    2001,
				Message: "Unauthorized",
			})
			return
		}
		ctx := context.WithValue(r.Context(), authCtxID, true)
		next.ServeHTTP(rw, r.WithContext(ctx))
	})
}

func (s *Server) dbUpsertDocument(document *Document) error {
	_, err := s.db.Exec("INSERT INTO books("+
		"document, percentage, progress, device, device_id_field, timestamp) "+
		"VALUES(?, ?, ?, ?, ?, ?) ON CONFLICT(document) DO UPDATE SET "+
		"percentage=?, progress=?, device=?, device_id_field=?, timestamp=?",
		document.Document, document.Percentage, document.Progress, document.Device, document.DeviceID, document.Timestamp,
		document.Percentage, document.Progress, document.Device, document.DeviceID, document.Timestamp)
	return err
}

func (s *Server) dbGetDocument(doc_id string) (*Document, error) {
	row := s.db.QueryRow("SELECT "+
		"document, percentage, progress, device, device_id_field, timestamp FROM books "+
		"WHERE document=?", doc_id)
	if row.Err() != nil {
		return nil, row.Err()
	}

	doc := Document{}
	row.Scan(&doc.Document, &doc.Percentage, &doc.Progress,
		&doc.Device, &doc.DeviceID, &doc.Timestamp)
	return &doc, nil
}

func (s *Server) Auth(rw http.ResponseWriter, r *http.Request) {
	if r.Context().Value(authCtxID).(bool) == true {
		rw.WriteHeader(http.StatusOK)
		json.NewEncoder(rw).Encode(Authed{
			Code:    200,
			Message: "OK",
		})
		return
	}

	rw.WriteHeader(http.StatusUnauthorized)
	json.NewEncoder(rw).Encode(Authed{
		Code:    2001,
		Message: "Unauthorized",
	})
}

func (s *Server) ProgressUpdate(rw http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPut {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
	doc := Document{}
	json.NewDecoder(r.Body).Decode(&doc)
	doc.Timestamp = time.Now().Unix()
	if err := s.dbUpsertDocument(&doc); err != nil {
		log.Printf("Error upserting: %s", err)
		return
	}
	json.NewEncoder(rw).Encode(UpdateResponse{
		Document:  doc,
		Timestamp: doc.Timestamp,
	})
}

func (s *Server) DocumentProgress(rw http.ResponseWriter, r *http.Request) {
	doc_id, ok := mux.Vars(r)["document"]
	if !ok {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
	doc, err := s.dbGetDocument(doc_id)
	if err != nil {
		log.Println("Error getting document:", err)
		rw.WriteHeader(http.StatusOK)
		rw.Write([]byte("{}"))
	}
	json.NewEncoder(rw).Encode(doc)
}

func (s *Server) Health(rw http.ResponseWriter, r *http.Request) {
	rw.WriteHeader(http.StatusOK)
}

func main() {
	configPath := flag.String("config", "/etc/gokosync.toml", "Path to config file")
	flag.Parse()
	config := Config{}
	if _, err := toml.DecodeFile(*configPath, &config); err != nil {
		log.Fatal(err)
	}

	db, err := sql.Open("sqlite", config.DBPath)
	if err != nil {
		log.Fatal(err)
	}
	if err := MigrateDB(db); err != nil {
		log.Fatal(err)
	}

	r := mux.NewRouter()
	s := Server{
		config: config,
		db:     db,
	}

	r.Handle("/users/auth", s.AuthMiddleware(http.HandlerFunc(s.Auth)))
	r.Handle("/syncs/progress", s.AuthMiddleware(http.HandlerFunc(s.ProgressUpdate)))
	r.Handle("/syncs/progress/{document}",
		s.AuthMiddleware(http.HandlerFunc(s.DocumentProgress)))
	r.HandleFunc("/healthcheck", s.Health)
	srv := &http.Server{
		Handler:      r,
		Addr:         "127.0.0.1:8323",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}

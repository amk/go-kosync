# gokosync

Go implementation of the koreader progress sync server.

## Build

`go build -o gokosync server.go`

## Config

Config file is a toml file with the following required options

```toml
username="<username>"
md5token="<md5 hash of the users password>"
db_path="<path to the sqlite database>"
```

